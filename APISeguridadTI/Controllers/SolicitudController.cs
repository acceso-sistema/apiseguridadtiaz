﻿using APISeguridadTI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace APISeguridadTI.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/Solicitud")]
    public class SolicitudController : ApiController
    {
        [Authorize]
        [HttpPost]
        [Route("GetEmailJefeInmediato")]
        public IHttpActionResult GetEmailJefeInmediato(EntryJefeInmediato Entry)
        {
            Bus bus = new Bus();
            try
            {
                return Ok(bus.GetEmailJefeInmediato(Entry));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
        [Authorize]
        [HttpPost]
        [Route("GetSubAreasGestionTiempos")]
        public IHttpActionResult GetSubAreasGestionTiempos(EntrySubareas Entry)
        {
            Bus bus = new Bus();
            try
            {
                return Ok(bus.GetSubAreasGestionTiempos(Entry));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
        [Authorize]
        [HttpPost]
        [Route("GetAreasGestionTiempos")]
        public IHttpActionResult GetAreasGestionTiempos(EntryAreas Entry)
        {
            Bus bus = new Bus();
            try
            {
                return Ok(bus.GetAreasGestionTiempos(Entry));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
        [Authorize]
        [HttpPost]
        [Route("GetGerenciasGestionTiempos")]
        public IHttpActionResult GetGerenciasGestionTiempos(EntryGerencias Entry)
        {
            Bus bus = new Bus();
            try
            {
                return Ok(bus.GetGerenciasGestionTiempos(Entry));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
        [Authorize]
        [HttpPost]
        [Route("GetSistemaPerfilList")]
        public IHttpActionResult GetSistemaPerfilList(EntrySistemaPerfil Entry)
        {
            Bus bus = new Bus();
            try
            {
                return Ok(bus.GetSistemaPerfilList(Entry));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
        [Authorize]
        [HttpPost]
        [Route("AprobacionSolicitud")]
        public IHttpActionResult AprobacionSolicitud(EntryAprobarSolicitud Entry)
        {
            Bus bus = new Bus();
            try
            {
                return Ok(bus.AprobacionSolicitud(Entry));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
        [Authorize]
        [HttpPost]
        [Route("CambiarPasswordGestionTiempos")]
        public IHttpActionResult CambiarPasswordGestionTiempos(EntryCambiarContrasena Entry)
        {
            Bus bus = new Bus();
            try
            {
                return Ok(bus.CambiarPasswordGestionTiempos(Entry));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
        [Authorize]
        [HttpPost]
        [Route("CambiarPasswordCuentasXPagar")]
        public IHttpActionResult CambiarPasswordCuentasXPagar(EntryCambiarContrasena Entry)
        {
            Bus bus = new Bus();
            try
            {
                return Ok(bus.CambiarPasswordCuentasXPagar(Entry));
            }
            catch (Exception e)
            {
                return Ok(Helpers.GetException(e));
            }
        }
    }
}
