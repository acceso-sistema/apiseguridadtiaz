﻿using Newtonsoft.Json;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace APISeguridadTI.Models
{
    public class Bus
    {
        public List<ResponseSistemaPerfil> GetSistemaPerfilList(EntrySistemaPerfil entry)
        {
            Dao Dao = null;
            List<ResponseSistemaPerfil> response = null;
            try
            {
                Dao = new Dao();
                if (entry.Sistema == "SISTEMA GESTION TIEMPOS")
                {
                    response = GetSistemaPerfilGestionTiemposList(entry);
                }
                else if (entry.Sistema == "SISTEMA COTIZADOR")
                {
                    response = Dao.GetSistemaPerfilCotizacionList(entry);
                }
                else if (entry.Sistema == "CTASXPAGAR")
                {
                    response = GetSistemaPerfilCuentasXPagarList(entry);
                }
            }
            catch
            {
                throw;
            }
            return response;
        }
        private List<ResponseSistemaPerfil> GetSistemaPerfilGestionTiemposList(EntrySistemaPerfil entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/GetSistemaPerfilGestionTiemposList", l_token);
                var json_data = JsonConvert.DeserializeObject<List<ResponseSistemaPerfil>>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }
        public Response AprobacionSolicitud(EntryAprobarSolicitud entry)
        {
            Dao Dao = null;
            List<SolicitudModel> response = null;
            Response rpt = null;
            try
            {
                Dao = new Dao();
                response = Dao.GetSolicitudToApiList(entry);
                foreach (SolicitudModel Solicitud in response)
                {
                    if (Solicitud.Tipo == "LA")
                    {
                        if (Solicitud.DescripcionSist == "SISTEMA GESTION TIEMPOS")
                        {
                            rpt = AprobarSolicitudGestionTiempos(Solicitud);
                        }
                        else if (Solicitud.DescripcionSist == "SISTEMA COTIZADOR")
                        {
                            rpt = Dao.AprobarSolicitudCotizacion(Solicitud);
                        }
                        else if (Solicitud.DescripcionSist == "CTASXPAGAR")
                        {
                            rpt = AprobarSolicitudCuentasXPagar(Solicitud);
                        }
                    } else if (Solicitud.Tipo == "EA")
                    {
                        if (Solicitud.DescripcionSist == "SISTEMA GESTION TIEMPOS")
                        {
                            rpt = RechazarSolicitudGestionTiempos(Solicitud);
                        }
                        else if (Solicitud.DescripcionSist == "SISTEMA COTIZADOR")
                        {
                            rpt = Dao.RechazarSolicitudCotizacion(Solicitud);
                        }
                        else if (Solicitud.DescripcionSist == "CTASXPAGAR")
                        {
                            rpt = RechazarSolicitudCuentasXPagar(Solicitud);
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return rpt;
        }
        private Response AprobarSolicitudGestionTiempos(SolicitudModel entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/AprobarSolicitudGestionTiempos", l_token);
                var json_data = JsonConvert.DeserializeObject<Response>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }
        private Response RechazarSolicitudGestionTiempos(SolicitudModel entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/RechazarSolicitudGestionTiempos", l_token);
                var json_data = JsonConvert.DeserializeObject<Response>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }
        public ResponseJefeInmediato GetEmailJefeInmediato(EntryJefeInmediato entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/GetEmailJefeInmediato", l_token);
                var json_data = JsonConvert.DeserializeObject<ResponseJefeInmediato>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }
        public List<ResponseSubareas> GetSubAreasGestionTiempos(EntrySubareas entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/GetSubAreasGestionTiempos", l_token);
                var json_data = JsonConvert.DeserializeObject<List<ResponseSubareas>>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }
        public List<ResponseAreas> GetAreasGestionTiempos(EntryAreas entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/GetAreasGestionTiempos", l_token);
                var json_data = JsonConvert.DeserializeObject<List<ResponseAreas>>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }
        public List<ResponseGerencias> GetGerenciasGestionTiempos(EntryGerencias entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/GetGerenciasGestionTiempos", l_token);
                var json_data = JsonConvert.DeserializeObject<List<ResponseGerencias>>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }

        private Response AprobarSolicitudCuentasXPagar(SolicitudModel entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/AprobarSolicitudCuentasXPagar", l_token);
                var json_data = JsonConvert.DeserializeObject<Response>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }
        private Response RechazarSolicitudCuentasXPagar(SolicitudModel entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/RechazarSolicitudCuentasXPagar", l_token);
                var json_data = JsonConvert.DeserializeObject<Response>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }
        private List<ResponseSistemaPerfil> GetSistemaPerfilCuentasXPagarList(EntrySistemaPerfil entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/GetSistemaPerfilCuentasXPagarList", l_token);
                var json_data = JsonConvert.DeserializeObject<List<ResponseSistemaPerfil>>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }
        public Response CambiarPasswordGestionTiempos(EntryCambiarContrasena entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/CambiarPasswordGestionTiempos", l_token);
                var json_data = JsonConvert.DeserializeObject<Response>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }
        public Response CambiarPasswordCuentasXPagar(EntryCambiarContrasena entry)
        {
            try
            {
                /* Inicio Ejecutar API */
                var my_jsondata_login = new
                {
                    Username = WebConfigurationManager.AppSettings["Usuario_api_seguridad_ti_local"].ToString(),
                    Password = WebConfigurationManager.AppSettings["Password_api_seguridad_ti_local"].ToString()
                };
                var l_url = WebConfigurationManager.AppSettings["URL_API_SEGURIDAD_TI_LOCAL"].ToString();
                string l_token = Helpers.executeAPIObject(my_jsondata_login, l_url + "/api/login/authenticate", "");
                l_token = l_token.Replace("\"", "");

                var my_jsondata_data = entry;

                string l_return_api = Helpers.executeAPIObject(my_jsondata_data, l_url + "/api/SolicitudLocal/CambiarPasswordCuentasXPagar", l_token);
                var json_data = JsonConvert.DeserializeObject<Response>(l_return_api);

                /* Fin Ejecutar API */
                return json_data;
                //return Json(json_data, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
            }
        }
    }
}