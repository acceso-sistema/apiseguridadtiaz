﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace APISeguridadTI.Models
{
    public class Dao
    {
        public List<ResponseSistemaPerfil> GetSistemaPerfilCotizacionList(EntrySistemaPerfil entry)
        {
            DataTable l_dt = null;
            List<ResponseSistemaPerfil> response;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    response = new List<ResponseSistemaPerfil>();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Coti_List_Sistema_Perfil";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_user", entry.CodiUser));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_tipo", entry.Tipo));
                    da.Fill(l_dt);
                }

                foreach (DataRow row in l_dt.Rows)
                {
                    ResponseSistemaPerfil rsp = new ResponseSistemaPerfil();
                    rsp.CodiPerf = Convert.ToDecimal(row["CODI_PERF"].ToString());
                    rsp.Descripcion = row["DESCRIPCION"].ToString();
                    response.Add(rsp);
                }
            }
            catch
            {
                throw;
            }
            return response;
        }
        public List<ResponseSistemaPerfil> GetSistemaPerfilCuentasXPagarList(EntrySistemaPerfil entry)
        {
            DataTable l_dt = null;
            List<ResponseSistemaPerfil> response;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    response = new List<ResponseSistemaPerfil>();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Ctas_List_Sistema_Perfil";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(l_dt);
                }

                foreach (DataRow row in l_dt.Rows)
                {
                    ResponseSistemaPerfil rsp = new ResponseSistemaPerfil();
                    rsp.CodiPerf = Convert.ToDecimal(row["CODI_PERF"].ToString());
                    rsp.Descripcion = row["DESCRIPCION"].ToString();
                    response.Add(rsp);
                }
            }
            catch
            {
                throw;
            }
            return response;
        }
        public string GetNombreTrabajadorById(decimal CodiTrab)
        {
            DataTable l_dt = null;
            string nombre = "";
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("PersonasAdmin"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Pers_Datos_Trabajador_By_Id";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_trab", CodiTrab));
                    da.Fill(l_dt);
                    foreach (DataRow row_email in l_dt.Rows)
                    {
                        nombre = row_email["NOMBRE"].ToString();
                    }
                }
            }
            catch
            {
                throw;
            }
            return nombre;
        }
        public List<SolicitudModel> GetSolicitudToApiList(EntryAprobarSolicitud entry)
        {
            DataTable l_dt = null;
            List<SolicitudModel> response;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("SeguridadTI"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    response = new List<SolicitudModel>();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Ti_List_Solicitud_To_Api";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_soli", entry.CodiSoli));
                    da.Fill(l_dt);
                }

                foreach (DataRow row in l_dt.Rows)
                {
                    SolicitudModel rsp = new SolicitudModel();
                    rsp.CodiPerf = Convert.ToDecimal(row["CODI_PERF"].ToString());
                    rsp.CodiTrab = Convert.ToDecimal(row["CODI_TRAB"].ToString());
                    rsp.Jerarquia = row["JERARQUIA"].ToString();
                    rsp.Tipo = row["DESCRIPCION_CORTA"].ToString();
                    rsp.FichaSap = row["FICHA_SAP"].ToString();
                    rsp.Username = row["USUA_CREA"].ToString();
                    rsp.DescripcionSist = row["DESCRIPCION_SIST"].ToString();
                    rsp.Nombre = GetNombreTrabajadorById(Convert.ToDecimal(row["CODI_TRAB"].ToString()));
                    response.Add(rsp);
                }
            }
            catch
            {
                throw;
            }
            return response;
        }
        public Response AprobarSolicitudCotizacion(SolicitudModel entry)
        {
            DataTable l_dt = null;
            Response response;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    response = new Response();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Ins_Cotizacion_Perfil";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_user", entry.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_perf", entry.CodiPerf));
                    da.Fill(l_dt);
                }

                foreach (DataRow row in l_dt.Rows)
                {
                    response.Value = row["Value"].ToString();
                    response.Mensaje = row["Mensaje"].ToString();
                }
            }
            catch
            {
                throw;
            }
            return response;
        }
        public Response RechazarSolicitudCotizacion(SolicitudModel entry)
        {
            DataTable l_dt = null;
            Response response;
            try
            {
                using (SqlConnection conn = Helpers.ConnectToSql("Administracion"))
                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    conn.Open();
                    l_dt = new DataTable();
                    response = new Response();
                    da.SelectCommand = conn.CreateCommand();
                    da.SelectCommand.CommandText = "dbo.Sp_Del_Cotizacion_Perfil";
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pv_codi_user", entry.Username));
                    da.SelectCommand.Parameters.Add(new SqlParameter("@pn_codi_perf", entry.CodiPerf));
                    da.Fill(l_dt);
                }

                foreach (DataRow row in l_dt.Rows)
                {
                    response.Value = row["Value"].ToString();
                    response.Mensaje = row["Mensaje"].ToString();
                }
            }
            catch
            {
                throw;
            }
            return response;
        }
    }
}