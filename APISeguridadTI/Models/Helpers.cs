﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;

namespace APISeguridadTI.Models
{
    public class Helpers
    {
        public static SqlConnection ConnectToSql(string ps_name)
        {
            string ls_connection = System.Configuration.ConfigurationManager.ConnectionStrings[ps_name].ConnectionString;
            return new SqlConnection(ls_connection);
        }
        public static OracleConnection ConnectToOracle(string ps_name)
        {
            string ls_connection = System.Configuration.ConfigurationManager.ConnectionStrings[ps_name].ConnectionString;
            return new OracleConnection(ls_connection);
        }
        public static Response GetMessageCrud(DataTable p_dt)
        {
            string l_message = null;
            string l_code = "";
            //int l_code = 0;
            try
            {
                foreach (DataRow row in p_dt.Rows)
                {
                    l_message = row["Mensaje"].ToString();
                    l_code = row["Value"].ToString();
                    //l_code = Convert.ToInt32(row["Value"]);
                }
                return new Response() { Value = l_code, Mensaje = l_message , Other = "0"};
            }
            catch
            {
                throw;
            }
        }
        public static Response GetException(Exception p_e)
        {
            string l_message = p_e.Message;
            string l_exception = "An exception occurred.\n" +
                "- Type : " + p_e.GetType().Name + "\n" +
                "- Message: " + p_e.Message + "\n" +
                "- Stack Trace: " + p_e.StackTrace;
            Exception l_ie = p_e.InnerException;
            if (l_ie != null)
            {
                l_exception += "\n";
                l_exception += "The Inner Exception:\n" +
                    "- Exception Name: " + l_ie.GetType().Name + "\n" +
                    "- Message: " + l_ie.Message + "\n" +
                    "- Stack Trace: " + l_ie.StackTrace;
            }
            return new Response() { Value = "3000", Mensaje = l_message, Other = l_exception };
        }
        public static string executeAPI(string json_data, string url, string token)
        {
            var postString = json_data;
            byte[] data = UTF8Encoding.UTF8.GetBytes(postString);
            HttpWebRequest request;
            request = WebRequest.Create(url) as HttpWebRequest;

            request.Timeout = 10 * 10000;
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "application/json; charset=utf-8";
            if (token != "")
            {
                request.Headers.Add("Authorization", token);
            }
            Stream postStream = request.GetRequestStream();
            postStream.Write(data, 0, data.Length);

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            return reader.ReadToEnd();
        }
        public static string executeAPIObject(object json_data, string url, string token)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (var client = new HttpClient())
            {
                if (token != "")
                {
                    client.DefaultRequestHeaders.Add("authorization", token);
                }
                client.BaseAddress = new Uri(url);
                //HTTP POST
                var postTask = client.PostAsJsonAsync("", json_data);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var ff = result.Content.ReadAsStringAsync().Result;
                    return ff;
                }
                else
                {
                    return "ERROR";
                }
            }
        }
    }
}