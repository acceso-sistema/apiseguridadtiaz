﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APISeguridadTI.Models
{
    public class Response
    {
        public string Value { get; set; }
        public string Mensaje { get; set; }
        public string Other { get; set; }
    }
    public class EntryJefeInmediato
    {
        public decimal CodiTrab { get; set; }
    }
    public class ResponseJefeInmediato
    {
        public string Email { get; set; }
        public string Username { get; set; }
    }
    public class EntrySubareas
    {
        public string FichaSap { get; set; }
        public string Tipo { get; set; }
        public decimal CodiPerf { get; set; }
        public decimal CodiArea { get; set; }
    }
    public class ResponseSubareas
    {
        public decimal CodiSuba { get; set; }
        public string Descripcion { get; set; }
        public string Jerarquia { get; set; }
    }
    public class EntryAreas
    {
        public string FichaSap { get; set; }
        public string Tipo { get; set; }
        public decimal CodiPerf { get; set; }
        public decimal CodiGere { get; set; }
    }
    public class ResponseAreas
    {
        public decimal CodiArea { get; set; }
        public string Descripcion { get; set; }
    }
    public class EntryGerencias
    {
        public string FichaSap { get; set; }
        public string Tipo { get; set; }
        public decimal CodiPerf { get; set; }
    }
    public class ResponseGerencias
    {
        public decimal CodiGere { get; set; }
        public string Descripcion { get; set; }
    }    
    public class EntrySistemaPerfil
    {
        public string Sistema { get; set; }
        public string CodiUser { get; set; }
        public string Tipo { get; set; }
    }
    public class ResponseSistemaPerfil
    {
        public decimal CodiPerf { get; set; }
        public string Descripcion { get; set; }
    }
    public class EntryAprobarSolicitud
    {
        public decimal CodiSoli { get; set; }
    }
    public class SolicitudModel
    {
        public decimal CodiPerf { get; set; }
        public string Jerarquia { get; set; }
        public string Tipo { get; set; }
        public decimal CodiTrab { get; set; }
        public string FichaSap { get; set; }
        public string Username { get; set; }
        public string DescripcionSist { get; set; }
        public string Nombre { get; set; }
    }
    public class EntryCambiarContrasena
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}